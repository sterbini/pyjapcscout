"""
Documentation for the pyjapcscout package

"""

__version__ = "0.0.1.dev0"

import warnings

from ._japc_scout import PyJapcScout
from ._japc_scout import PyJapcExt
from ._japc_data  import PyJapcScoutData
from ._japc_converter import PyJapcScoutConverter



__cmmnbuild_deps__ = [
    {"product": "ctf-customjars-matlab", "groupId": "cern.ctf"}, 
]
