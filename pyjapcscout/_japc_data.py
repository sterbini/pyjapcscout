"""
Data frame used by PyJapcScout to return data from the user

Guido Sterbini - March 2021

"""
import os
import numpy as np
import pandas as pd
import scipy.io
import pickle
from datetime import datetime
from pathlib import Path

############################################
# add the _ method to the Series
def _myFun(self,x,mykey):
    if x==None:
        return None
    else:
        return x[mykey]

def _key(self, mykey):
    aux=self.apply(lambda x: _myFun(self,x,mykey)) 
    if not (mykey == "value"):
        aux.name= aux.name +'#'+ mykey
    return aux

pd.Series._=_key 
############################################

class PyJapcScoutData(object):
    '''
    # will contain a dictionary with some additional methods to save/load/transform data
    dict('BBQ/Acquisition', 'Magnet/Current', 'Setting', ....)
    # and inside there will be another dict with:
    dict('header', 'value', 'exception')

    I implemented those formats
    
    myDict1 = {'BBQ/Acquisition': {'value':{'property1':np.random.rand(100)},'header':{'acqStamp':1,'cycleStamp':1622969203.2000}},
         'Magnet1/Current#value': {'value':np.random.rand(1), 'header':{'acqStamp':1,'cycleStamp':1622969203.2000}},
         'Magnet2/Current#value': {'value':np.random.rand(1),'header':{'acqStamp':1,'cycleStamp':1622969203.200}}}

    myDict2 = {'BBQ/Acquisition': {'value':{'property1':np.random.rand(100)},'header':{'acqStamp':1,'cycleStamp':1622969204.4000}},
             'Magnet2/Current#value': {'value':np.random.rand(1),'header':{'acqStamp':1,'cycleStamp':1622969204.400}}}

    myDict3 = {'BBQ/Acquisition': {'value':{'property1':np.random.rand(100)},'header':{'acqStamp':1,'cycleStamp':1622969205.6000}},
             'Magnet1/Current#value': {'value':np.random.rand(1), 'header':{'acqStamp':1,'cycleStamp':1622969205.6000}}}

    myDict4 = {
             'Magnet1/Current#value': {'value':np.random.rand(1), 'header':{'acqStamp':1,'cycleStamp':1622969206.8000}},
             'Magnet2/Current#value': {'value':np.random.rand(1),'header':{'acqStamp':1,'cycleStamp':1622969206.800}}}

    '''
    
    def __init__(self, mydict = None, indexing= 'cycleStamp'):
        '''
        Convert the dictionary or the list of dictionary in df. 
        The df should be the main object of the class, all methods are around it. 
        '''
        if type(mydict)==dict:
            self._dict = [mydict]
        elif type(mydict)==list:
            self._dict = mydict
        elif type(mydict)==pd.DataFrame:
            self._dict = mydict
        elif mydict == None:
            self._dict = None
        else:
            raise TypeError('Only dict or list of dict are accepted arguments')
            
        self._indexing = indexing    
      
        if mydict == None:
            self.df = None           
        else:
            if type(mydict)==pd.DataFrame:
                self.df = mydict
            else:
                self.df = self._data2pd()
    
    def _getIndex(self, mydict):
        '''Given a list of dictionaries it defines a single cyclestamp or acqstamp'''
        return np.max([mydict[ii]['header'][self._indexing] for ii in mydict.keys()])
        
    def _dict2pd(self,mydict):
        return pd.DataFrame([mydict], index=[self._getIndex(mydict)])
        
    def _data2pd(self):
        '''Given a list of dictionaries it gets the df'''
        return pd.concat(list(map(self._dict2pd,self._dict)))
    
    def pd2dict(self, myDF,  indexing= 'cycleStamp'):
        '''Given a list of dictionaries it gets the df'''
        return myDF.to_dict(orient='list')
    
    def _getFilename(self):
        '''Get the standard cyclestamp'''
        return datetime.fromtimestamp(np.max(self.df.index.values)).strftime("%Y.%m.%d.%H.%M.%S.%f")
    
    def save(self, folderPath = None, filename = None, fileFormat='parquet'):
        if filename == None:
            filename = self._getFilename()
        Path(folderPath).mkdir(parents=True, exist_ok=True)
        filename = os.path.join(folderPath, filename)
        if fileFormat == 'parquet':
            self.df.to_parquet(filename+'.parquet')
        elif fileFormat == 'json':
            self.df.to_json(filename+'.json')
        elif fileFormat == 'pickle':
            self.df.to_pickle(filename+'.pkl')
        elif fileFormat == 'pickledict':
            with open(filename+'.pkl', 'wb') as handle:
                pickle.dump(self._dict, handle, protocol=pickle.HIGHEST_PROTOCOL)
        elif fileFormat == 'mat':
            raise ValueError('MAT format not yet supported')
            scipy.io.savemat(filename+'.mat', self._dict)
        else:
            raise ValueError('Unknown file format')

    def load(self, filename, fileFormat='parquet'):
        if fileFormat == 'parquet':
            self.df=pd.read_parquet(filename)
        elif fileFormat == 'json':
            self.df=pd.read_json(filename)
        elif fileFormat == 'pickle':
            self.df=pd.read_pickle(filename)
        elif fileFormat == 'pickledict':
            with open(filename, 'rb') as handle:
                self._dict = pickle.load(handle)
            self.df = self._data2pd()
        elif fileFormat == 'mat':
            raise ValueError('MAT format not yet supported')
            print('TODO: compatibility with MATLAB generated files?!')
            self._dict = scipy.io.loadmat(filename)
            self.df = self._data2pd()
        else:
            raise ValueError('Unknown file format')
            
    def get_value(self, column, key1= None):
        if key1==None:
            return pd.DataFrame(self.df[column]._('value'))
        else:
            return pd.DataFrame(self.df[column]._('value')._(key1))
    
    def get_header(self, column, key1= None):
        if key1==None:
            return pd.DataFrame(self.df[column]._('header'))
        else:
            return pd.DataFrame(self.df[column]._('header')._(key1))
        
    def get_schema(self):
        print('################################')
        print(f'DataFrame: {len(self.df)} rows')
        print('################################\n')


        for ii in self.df.columns:
            aux=self.df[ii].dropna()
            print(f'{ii}: {len(aux)} non-null rows')
            for jj in aux.iloc[0].keys():
                print(f'  - {jj}: {type(aux.iloc[0][jj])}')
                if type(aux.iloc[0][jj])==dict:
                    for kk in aux.iloc[0][jj].keys():
                        print(f'    -- {kk}:\t\t {type(aux.iloc[0][jj][kk])}')
            print()
