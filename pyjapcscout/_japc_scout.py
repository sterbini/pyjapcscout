"""
PyJapcScout is a dressing of PyJapc that uses a different group subscription strategy
and implements some "standard" methods to save data.

"""

from pyjapc import PyJapc
import jpype as jp
import six
from datetime import datetime
from ._japc_converter import PyJapcScoutConverter
from ._japc_data  import PyJapcScoutData

######################################################
# Single parameter acquisition multiple time per BP
######################################################

class PyJapcScout(object):
    def __init__(self, incaAcceleratorName = None, defaultSelector = "", noSet=False, timeZone="utc", unixtime=False,
                 logLevel=None, **kwargs):
        """ 
        Create a simple PyJapc object to Initialize JVM and Inca. By default no 'incaification' is performed, 
        but you can still use 'auto' to use the provided selector (if not empty) to find the right inca server.

        Note: Incaification can only happen at the first creation of this object (or any other PyJapc object created before...).
        """

        self._mainPyJapc = PyJapc(selector=defaultSelector, incaAcceleratorName=incaAcceleratorName, noSet=noSet, timeZone=timeZone, logLevel=logLevel)
        self._unixtime = unixtime
        self.setDefaultSelector(defaultSelector)

        # create here object that handle Java <-> Python conversion for this object
        self._dataConverter = PyJapcScoutConverter()    

    def setDefaultSelector(self, selector):
        self._defaultSelector = selector

    def rbacLogin(self, username=None, password=None, loginDialog=False, readEnv=True):
        self._mainPyJapc.rbacLogin(self, username=username, password=password, loginDialog=loginDialog, readEnv=readEnv)
    def rbacLogout(self):
        self._mainPyJapc.rbacLogout()
    
    # A couple of functions for the simplest ever cases: get and set an actual value (e.g. a DC power converter current value)
    def getSimpleValue(self, parameterName, selectorOverride = None):
        values = self.getValues([parameterName], selectorOverride)
        return values[parameterName]['value']
    def setSimpleValue(self, parameterName, value, selectorOverride = None, dataFilter = None):
        # create a temporary dict object
        auxData = {parameterName: {'value': value}}
        # use wider method
        self.setValues([parameterName], auxData, selectorOverride, dataFilter)

    def getValues(self, parameterNames, selectorOverride = None):
        """
        getValue(self, parameterNames, selectorOverride = None)
        parameterNames is a list of parameters
        it returns a dict with all data inside
        """
        # Decide which selector to use:
        if selectorOverride == None:
            selector = self._defaultSelector
        else:
            selector = selectorOverride
        
        # create a dict object and fill it
        output = dict()
        for parameterName in parameterNames:
            # Make a standard get from local PyJapc object
            value = self._mainPyJapc.getParam(parameterName, noPyConversion = True, timingSelectorOverride = selector)
            # convert
            value = self._dataConverter.JAcquiredValueToPy(value)
            # store
            output[parameterName] = value

        return output

    def setValues(self, parameterNames, values, selectorOverride = None, dataFilter = None):
        """ 
        parameterNames is a list of parameters to be set
        values must be a dict of dicts that contains data for all parameterNames elements
        - each dict, must contain a "value" key with the data you want to set
        - the latter can also be a dict in case of full property being set. In this case, the 
        """

        # Decide which selector to use:
        if selectorOverride == None:
            selector = self._defaultSelector
        else:
            selector = selectorOverride

        for parameterName in parameterNames:
            # actual data to be set:
            auxData = values[parameterName]['value']
            # get parameter descriptor
            auxDescriptor = self._mainPyJapc._getJapcPar(parameterName).getValueDescriptor()
            # convert to Java: 
            auxData = self._dataConverter.PyToJValue(auxData, auxDescriptor)
            # auxData should now be already a "cern.japc.value.ParameterValue", so PyJapc should not try to crate a new one...
            self._mainPyJapc.setParam(parameterName, auxData, timingSelectorOverride = selector, dataFilterOverride = dataFilter, checkDims=False, dtype=None)

    def createMonitor(self, parameterNames, onValueReceived=None, selectorOverride = None,
                        groupStrategy = 'standard',
                        allowManyUpdatesPerCycle=False, strategyTimeout=1200, forceGetOnChangeAndConstantValues=False, # only for groupStrategy=extended
                        **kwargs):
        """
        Creates a PyJapcScoutMonitor which allows one to control the monitoring of a set of parameters

        groupStrategy: 
            'standard' -> simple PyJapc group subscription 
            'extended' -> using PyJapcExt which uses CTF-based 
            'multiple' -> multiple independent subscriptions that call back the same function, no attempt to sync data
        """
        # Decide which selector to use:
        if selectorOverride == None:
            selector = self._defaultSelector
        else:
            selector = selectorOverride
        
        # Create and return a monitoring object
        return self.PyJapcScoutMonitor(selector, parameterNames, onValueReceived=onValueReceived, unixtime=self._unixtime,
                        groupStrategy = groupStrategy,
                        allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout, forceGetOnChangeAndConstantValues=forceGetOnChangeAndConstantValues, # only for groupStrategy=extended
                        **kwargs)
    

    class PyJapcScoutMonitor(object):
        def __init__(self, selector, parameterNames, onValueReceived=None, unixtime=False, timeZone="utc",
                        groupStrategy = 'standard',
                        allowManyUpdatesPerCycle=False, strategyTimeout=1200, forceGetOnChangeAndConstantValues=False, # only for groupStrategy=extended
                        **kwargs):
            """
            PyJapcScoutMonitor
            """
            # just initialize some vars
            self.lastData = dict()

            self.saveData = False
            self.saveDataPath = "."
            self.saveDataFormat = "pickle"

            # for synchronization purposes... badly implemented
            self.synchronizedCalls = True
            self._isTreatingData = False

            # mandatory properties fields
            self._userCallback = onValueReceived
            self._myPyJapc = None

            # let's save those options here - just in case
            self._unixtime = unixtime
            self._timeZone = timeZone
            
            # create here object that handle Java -> Python conversion for this object
            self._dataConverter = PyJapcScoutConverter()

            # force parameterNames to be a list
            if isinstance(parameterNames, six.string_types):
                parameterNames = [parameterNames]
            
            # depending on the strategy chosen, a different pyjapc object will be created and used
            # NOTE: I am afraid several options below are not needed as not taken into account by PyJapc if noPyConversion=True..., getHeader=True, unixtime=unixtime, timeZone = timeZone ...
            if groupStrategy == 'standard':
                # make a standard group subscription with pyJapc
                self._myPyJapc = PyJapc(selector, timeZone = timeZone)
                self._myPyJapc.subscribeParam(parameterNames, onValueReceived=self._masterCallback, 
                        getHeader=True, noPyConversion=True, unixtime=unixtime, **kwargs)
            elif groupStrategy == 'extended':
                # Create a PyJapcExt object (using CTF-like subscription strategy)
                self._myPyJapc = PyJapcExt(selector, timeZone = timeZone)
                self._myPyJapc.subscribeParamExt(parameterNames, onValueReceived=self._masterCallback, 
                        getHeader=True, noPyConversion=True, unixtime=unixtime, 
                        allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout, forceGetOnChangeAndConstantValues=forceGetOnChangeAndConstantValues,
                        **kwargs)
            elif groupStrategy == 'multiple':
                # make several subscriptions within the same pyjapc object.
                # Still, "force" to use group subscription for each parameter to simplify the code of PyJapcScoutMonitor class
                self._myPyJapc = PyJapc(selector, timeZone = timeZone)
                for parameterName in parameterNames:
                    self._myPyJapc.subscribeParam([parameterName], onValueReceived=self._masterCallback, 
                        getHeader=True, noPyConversion=True, unixtime=unixtime, **kwargs)
            else:
                raise ValueError('Unknown group subscription strategy')
            
        def startMonitor(self):
            self._myPyJapc.startSubscriptions()
        def stopMonitor(self):
            self._myPyJapc.stopSubscriptions()
        
        def _masterCallback(self, values):

            # just some lock mechanism to ensure data synchronisation
            if self.synchronizedCalls & self._isTreatingData :
                print("The monitor is already working on a previous acquisition... You will be loosing this dataset...")
                return
            else:
                self._isTreatingData = True

            # create dict to hold all acquired data
            outputData = dict()
            # by construction, values is always a Java array of FailSafeParameterValues.
            #   treat each of them and store them
            for failsSafeValue in values:
                outputData[failsSafeValue.getParameterName()] = self._dataConverter.JAcquiredValueToPy(failsSafeValue)
            
            # store it as last dataset
            self.lastData = outputData

            # save the data if requested
            if self.saveData:
                try:
                    filename = datetime.now().strftime("%Y.%m.%d.%H.%M.%S.%f")
                    # create here a PyJapcScoutData and use it to convert/save data
                    dataToSave = PyJapcScoutData(outputData)
                    dataToSave.save(folderPath = self.saveDataPath, filename=filename, fileFormat=self.saveDataFormat)
                except:
                    print('PyJapcScout::Some error saving the data... please debug it... I will continue')

            # Now I can call the user with all the data structure
            if self._userCallback is not None:
                try:
                    # TODO: make it better... 
                    self._userCallback(outputData, self)
                except:
                    print('PyJapcScout::Some error calling user function... please debug it... I will continue')

            # If I am here, all is good - I can close
            self._isTreatingData = False
        

class PyJapcExt(PyJapc):
    """
    Extension of PyJapc using CTF-based subscription strategy
    """
    def __init__(self, *args, **kwargs):
        super(PyJapcExt, self).__init__(*args, **kwargs)
     
    def subscribeParamExt(self, parameterName, onValueReceived=None, onException=None, getHeader=False,
                       noPyConversion=False, unixtime=False,
                       allowManyUpdatesPerCycle=False, strategyTimeout=1200, forceGetOnChangeAndConstantValues=False,
                       **kwargs):
        # --------------------------------------------------------------------
        # Create a unique string key for this subscription
        # --------------------------------------------------------------------
        parameterKey = self._getDictKeyFromParameterName(parameterName)
        try:
            sel = kwargs['timingSelectorOverride']
        except KeyError:
            sel = None
        parameterKey = self._transformSubscribeCacheKey(parameterKey=parameterKey,
                                                        selector=sel)
        
        # Get the (cached) JAPC parameter object
        ################################################################
        ###### Davide - start modification - Basically re-implemnting locally 
        ######          "self._getJapcPar" using my strategy from CTF
        #par = self._getJapcPar(parameterName)
        parameterKey = self._getDictKeyFromParameterName(parameterName)

        # Davide: one might want to subscribe to same parameter, same selector, but different strategy - need to comment out this
        #if parameterKey in self._paramDict:
        #    # Parameter object exists already in dict
        #    p = self._paramDict[parameterKey]
        #else:
        # Make group subscription in all cases - single parameter or many parameters - there is no difference!

        # Create a new ParameterGroup object, populate it
        #p = jp.JPackage("cern").japc.core.spi.group.ParameterGroupImpl()
        strategy = jp.JPackage("cern").ctf.customjarsmatlab.GroupSubscriptionStrategyFactoryCTF(True)
        strategy.setFastStrategyAllowManyUpdatesPerCycle(bool(allowManyUpdatesPerCycle))
        strategy.setFastStrategyForceGetOnChangeAndConstantValues(bool(forceGetOnChangeAndConstantValues)) 
        strategy.setFastStrategyTimeOut(int(strategyTimeout)) # this should be a long...

        # create standard group Jobject with injected strategy
        p = jp.JPackage("cern").japc.core.spi.group.ParameterGroupImpl(strategy)
        for parName in parameterName:
            p.add(self._getJapcPar(parName))
        # Store the newly created object in the dict
        #self._paramDict[parameterKey] = p
        
        # To continue as before...
        par = p
        ######## Davide - end modification, same function as PyJapc 
        ################################################################
        
        lisJ = self._createValueListener(par=par,
                                        getHeader=getHeader,
                                        noPyConversion=noPyConversion,
                                        unixtime=unixtime,
                                        onValueReceived=onValueReceived,
                                        onException=onException)

        # --------------------------------------------------------------------
        # !!! Subscribe !!!
        # --------------------------------------------------------------------
        s = self._giveMeSelector(**kwargs)
        sh = par.createSubscription(s, lisJ)

        # --------------------------------------------------------------------
        # Add SubscriptionHandle to cache for later access
        # --------------------------------------------------------------------
        # Davide - this key is too weak now... it should be made differently...
        self._subscriptionHandleDict.setdefault(parameterKey, []).append(sh)

        return sh
