myDict1 = {'BBQ/Acquisition': {'value':{'property1':np.random.rand(100)},'header':{'acqStamp':1,'cycleStamp':1622969203.2000}},
        'Magnet1/Current#value': {'value':np.random.rand(1), 'header':{'acqStamp':1,'cycleStamp':1622969203.2000}},
         'Magnet2/Current#value': {'value':np.random.rand(1),'header':{'acqStamp':2,'cycleStamp':1622969203.200}}}

myDict2 = {'BBQ/Acquisition': {'value':{'property1':np.random.rand(100)},'header':{'acqStamp':1,'cycleStamp':1622969204.4000}},
        'Magnet1/Current#value': None,
         'Magnet2/Current#value': {'value':np.random.rand(1),'header':{'acqStamp':3,'cycleStamp':1622969204.400}}}

myDict3 = {'BBQ/Acquisition': {'value':{'property1':np.random.rand(100)},'header':{'acqStamp':1,'cycleStamp':1622969205.6000}},
         'Magnet1/Current#value': {'value':np.random.rand(1), 'header':{'acqStamp':5,'cycleStamp':1622969205.6000}},
         'Magnet2/Current#value': None,}

myDict4 = {'BBQ/Acquisition': None,
        'Magnet1/Current#value': {'value':np.random.rand(1), 'header':{'acqStamp':1,'cycleStamp':1622969206.8000}},
         'Magnet2/Current#value': {'value':np.random.rand(1),'header':{'acqStamp':6,'cycleStamp':1622969206.800}}}
