### working example, but not working with parquet
from pyjapcscout import PyJapcScout
from pyjapcscout import PyJapcScoutData

myPyJapc = PyJapcScout(incaAcceleratorName='CTF', defaultSelector='SCT.USER.SETUP', noSet=False, timeZone="utc", unixtime=False, logLevel=None)
parameters = ['CA.BTV0125/Image', 'CA.BTV0125/Acquisition', 'CA.BHB0400/Acquisition#currentAverage']
values = myPyJapc.getValues(parameters,selectorOverride='SCT.USER.SETUP')
myData = PyJapcScoutData(values)
myData.save(folderPath = './data/', filename = 'testData', fileFormat='pickledict')

myData2 = PyJapcScoutData()
myData2.load('./data/testData.pkl', fileFormat='pickledict')


# this does not work
myData.save(folderPath = './data/', filename = 'testData', fileFormat='parquet')
