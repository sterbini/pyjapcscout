"""
Class used by PyJapcScout for data conversion from Java to Python and viceversa
"""

import jpype as jp
import datetime
import numpy as np
import pytz

class PyJapcScoutConverter(object):
    """
     %PyJapcScoutConverter
    # This class is used to speed up the extraction of data from Java
    # objects.
    %
    # The main idea is that the check of the type is taking too much time,
    # so I use instead the hashCode given by the Type class.
    %
    # Based on matlabJAPC extractor class

    + looking at https://gitlab.cern.ch/acc-logging-team/nxcals/-/blob/develop/common/src/main/java/cern/nxcals/api/converters/ImmutableDataToAvroConverter.java
    + looking at https://issues.cern.ch/browse/NXCALS-4620 for function list
    """

    def __init__(self):
        # Just initialize variables

        cern = jp.JPackage("cern")

        #from cern.japc.value.Type
        self.SIMPLE            = cern.japc.value.Type.SIMPLE.hashCode()
        self.MAP               = cern.japc.value.Type.MAP.hashCode()
        
        #from cern.japc.value.ValueType
        self.UNDEFINED         = cern.japc.value.ValueType.UNDEFINED.hashCode()
        self.BOOLEAN           = cern.japc.value.ValueType.BOOLEAN.hashCode()
        self.BYTE              = cern.japc.value.ValueType.BYTE.hashCode()
        self.DOUBLE            = cern.japc.value.ValueType.DOUBLE.hashCode()
        self.FLOAT             = cern.japc.value.ValueType.FLOAT.hashCode()
        self.INT               = cern.japc.value.ValueType.INT.hashCode()
        self.LONG              = cern.japc.value.ValueType.LONG.hashCode()
        self.SHORT             = cern.japc.value.ValueType.SHORT.hashCode()
        self.STRING            = cern.japc.value.ValueType.STRING.hashCode()
        self.BOOLEAN_ARRAY     = cern.japc.value.ValueType.BOOLEAN_ARRAY.hashCode()
        self.BYTE_ARRAY        = cern.japc.value.ValueType.BYTE_ARRAY.hashCode()
        self.DOUBLE_ARRAY      = cern.japc.value.ValueType.DOUBLE_ARRAY.hashCode()
        self.FLOAT_ARRAY       = cern.japc.value.ValueType.FLOAT_ARRAY.hashCode()
        self.INT_ARRAY         = cern.japc.value.ValueType.INT_ARRAY.hashCode()
        self.LONG_ARRAY        = cern.japc.value.ValueType.LONG_ARRAY.hashCode()
        self.SHORT_ARRAY       = cern.japc.value.ValueType.SHORT_ARRAY.hashCode()
        self.STRING_ARRAY      = cern.japc.value.ValueType.STRING_ARRAY.hashCode()
        self.BOOLEAN_ARRAY_2D  = cern.japc.value.ValueType.BOOLEAN_ARRAY_2D.hashCode()
        self.BYTE_ARRAY_2D     = cern.japc.value.ValueType.BYTE_ARRAY_2D.hashCode()
        self.DOUBLE_ARRAY_2D   = cern.japc.value.ValueType.DOUBLE_ARRAY_2D.hashCode()
        self.FLOAT_ARRAY_2D    = cern.japc.value.ValueType.FLOAT_ARRAY_2D.hashCode()
        self.INT_ARRAY_2D      = cern.japc.value.ValueType.INT_ARRAY_2D.hashCode()
        self.LONG_ARRAY_2D     = cern.japc.value.ValueType.LONG_ARRAY_2D.hashCode()
        self.SHORT_ARRAY_2D    = cern.japc.value.ValueType.SHORT_ARRAY_2D.hashCode()
        self.STRING_ARRAY_2D   = cern.japc.value.ValueType.STRING_ARRAY_2D.hashCode()
        self.ENUM              = cern.japc.value.ValueType.ENUM.hashCode()
        self.ENUM_SET          = cern.japc.value.ValueType.ENUM_SET.hashCode()
        self.DISCRETE_FUNCTION = cern.japc.value.ValueType.DISCRETE_FUNCTION.hashCode()
        self.DISCRETE_FUNCTION_LIST= cern.japc.value.ValueType.DISCRETE_FUNCTION_LIST.hashCode()
        self.ENUM_ARRAY        = cern.japc.value.ValueType.ENUM_ARRAY.hashCode()
        self.ENUM_SET_ARRAY    = cern.japc.value.ValueType.ENUM_SET_ARRAY.hashCode()
        self.ENUM_ARRAY_2D     = cern.japc.value.ValueType.ENUM_ARRAY_2D.hashCode()
        self.ENUM_SET_ARRAY_2D = cern.japc.value.ValueType.ENUM_SET_ARRAY_2D.hashCode()


    def JAcquiredValueToPy(self, acquiredData, unixtime=False, timeZone="utc"):
        header    = self._convertValueHeaderToPy(acquiredData.getHeader(), unixtime=unixtime, timeZone=timeZone)
        try:
            exception = self._convertValueExceptionToPy(acquiredData.getException())
        except:
            # hopefully we are dealing with a cern.japc.core.spi.AcquiredParameterValueImpl
            exception = ''

        if exception == '':
            value = self._convertValueValueToPy(acquiredData.getValue())
        else:
            print('Error occurred acquiring ' + acquiredData.getParameterName()+': ' + exception)
            value = self._createEmptyPyValueFromDesc(acquiredData.getDescriptor())
        
        return  {'value': value, 'header': header, 'exception': exception}
    
    def _convertValueExceptionToPy(self, valueException):
        if valueException == None:
            return ''
        else:
            return valueException.getMessage()

    def _convertValueHeaderToPy(self, valueHeader, unixtime=False, timeZone="utc"):
        """ Convert a `ValueHeader` object to a python dictionary 
        - Copy-Paste from PyJapc!
        """
        headerDict = dict()
        if unixtime:
            headerDict["acqStamp"]   = valueHeader.getAcqStamp()   / 1e9
            headerDict["cycleStamp"] = valueHeader.getCycleStamp() / 1e9
            headerDict["setStamp"]   = valueHeader.getSetStamp()   / 1e9
        else:
            if timeZone == "utc":
                timeZone = pytz.utc
            elif timeZone == "local":
                timeZone = pytz.timezone("Europe/Zurich")
            elif isinstance(timeZone, datetime.tzinfo) or timeZone is None:
                timeZone = timeZone
            else:
                self.log.warning("Unknown timeZone argument: {0}. Falling back on UTC time.".format(timeZone))
                timeZone = pytz.utc
            
            headerDict["acqStamp"] = datetime.datetime.fromtimestamp(valueHeader.getAcqStamp() / 1e9,
                                                                    tz=timeZone)
            headerDict["cycleStamp"] = datetime.datetime.fromtimestamp(valueHeader.getCycleStamp() / 1e9,
                                                                    tz=timeZone)
            headerDict["setStamp"] = datetime.datetime.fromtimestamp(valueHeader.getSetStamp() / 1e9,
                                                                    tz=timeZone)

        headerDict["isFirstUpdate"] = bool(valueHeader.isFirstUpdate())
        headerDict["isImmediateUpdate"] = bool(valueHeader.isImmediateUpdate())
        if valueHeader.getSelector() is not None:
            headerDict["selector"] = valueHeader.getSelector().toString()
        else:
            headerDict["selector"] = None
        return headerDict

    def _convertValueValueToPy(self, JValue):
        """
            %PyValue = _convertValueValueToPy(JValue)
            # it extract all the fields from a MAP or the value from a SIMPLE parameter value
        """

        #see if JValue is a map or a simple object  
        JValueTypeHash = JValue.getType().hashCode()
        if (JValueTypeHash == self.SIMPLE):
            PyValue = self._convertValueSimpleValueToPy(JValue)
        elif (JValueTypeHash == self.MAP):
            # since this is a big structure, I will give back a dict
            # with all the fields inside.
            PyValue = dict()
            
            # here the names of the fields
            allFieldNames = JValue.getNames()
            
            # now extract them all
            for fieldName in allFieldNames:
                ##for DEBUG
                #print('^--------------V')
                #print(fieldName)
                PyValue[fieldName] = self._convertValueSimpleValueToPy(JValue.get(fieldName))
        else:
            raise TypeError('PyJapcScoutExtractor::unknown Java Value type. Probably an OBJECT -> not supported')
        return PyValue
        
    def _convertValueSimpleValueToPy(self, JValue):
        """
        PyValue = _convertValueSimpleValueToPy(JValue)
           extracts a python understandable value from a Java Simple Parameter Value.

           All values will be numpy values (or arrays), or list/dict of numpy values (or arrays)
           Present implementation is very verbose/pedantic for academic reasons....
        """

        # get JAPC value type
        JType = JValue.getValueType().hashCode()
        
        # convert all value types
        ########## NUMERIC/SIMPLE -> TO NUMPY ###########
        if (JType == self.DOUBLE_ARRAY) or (JType == self.DOUBLE_ARRAY_2D):
            PyValue = np.array(JValue.getDoubles(), dtype=np.float64)
        elif (JType == self.DOUBLE ):
            PyValue = np.float64(JValue.getDouble())
            
        elif (JType == self.FLOAT_ARRAY) or (JType == self.FLOAT_ARRAY_2D):
            PyValue = np.array(JValue.getFloats(), dtype=np.float32)
        elif (JType == self.FLOAT ):
            PyValue = np.float32(JValue.getFloat())
            
        elif (JType == self.LONG_ARRAY) or (JType == self.LONG_ARRAY_2D):
            PyValue = np.array(JValue.getLongs(), dtype=np.int64)
        elif (JType == self.LONG ):
            PyValue = np.int64(JValue.getLong())
               
        elif (JType == self.INT_ARRAY) or (JType == self.INT_ARRAY_2D):
            PyValue = np.array(Value.getInts(), dtype=np.int32)
        elif (JType == self.INT ):
            PyValue = np.int32(JValue.getInt())

        elif (JType == self.SHORT_ARRAY) or (JType == self.SHORT_ARRAY_2D):
            PyValue = np.array(JValue.getShorts(), dtype=np.int16)
        elif (JType == self.SHORT ):
            PyValue = np.int16(JValue.getShort())
            
        elif (JType == self.BYTE_ARRAY) or (JType == self.BYTE_ARRAY_2D):
            PyValue = np.array(JValue.getBytes(), dtype=np.int8)
        elif (JType == self.BYTE ):
            PyValue = np.int8(JValue.getByte())
        
        elif (JType == self.BOOLEAN_ARRAY) or (JType == self.BOOLEAN_ARRAY_2D):
            PyValue = np.array(JValue.getBooleans(), dtype=np.bool_)
        elif (JType == self.BOOLEAN):
            PyValue = np.bool_(JValue.getBoolean())

        ########## STRING -> TO (LIST) STRING(S) ###########
        elif (JType == self.STRING_ARRAY) or (JType == self.STRING_ARRAY_2D):
            PyValue = list(JValue.getStrings())
        elif (JType == self.STRING):
            PyValue = str(JValue.getString())
        

        ########## ENUM -> TO (LIST or LIST of LISTs) dict{int: string} ###########
        elif (JType == self.ENUM_ARRAY) or (JType == self.ENUM_ARRAY_2D):
            PyValue = []
            for auxEnum in JValue.getEnumItems():
                PyValue.append( { int(auxEnum.getCode()): str(auxEnum.getString()) } )
        elif (JType == self.ENUM):
            auxEnum = JValue.getEnumItem()
            PyValue = { int(auxEnum.getCode()): str(auxEnum.getString()) }
            
        elif (JType == self.ENUM_SET_ARRAY) or (JType == self.ENUM_SET_ARRAY_2D):
            PyValue = []
            for auxEnumSet in JValue.getEnumItemSets():
                PyValue.append( { v.getCode():v.getString() for v in auxEnumSet.getEnumItemSet() } )
        elif (JType == self.ENUM_SET):
            PyValue = { v.getCode(): v.getString() for v in JValue.getEnumItemSet() }

        ########## DISCRETE_FUNCTION -> (LIST) dict{'X': np.float64, 'Y': np.float64}###########
        elif (JType == self.DISCRETE_FUNCTION):
            PyValue = dict()
            auxFunction = JValue.getDiscreteFunction()
            PyValue['X'] = np.array(auxFunction.getXArray(), dtype=np.float64)
            PyValue['Y'] = np.array(auxFunction.getYArray(), dtype=np.float64)
        elif (JType == self.DISCRETE_FUNCTION_LIST):
            PyValue = []
            for auxFunction in JValue.getDiscreteFunctionList().getFunctions():
                auxValue = dict()
                auxValue['X'] = np.array(auxFunction.getXArray(), dtype=np.float64)
                auxValue['Y'] = np.array(auxFunction.getYArray(), dtype=np.float64)
                PyValue.append(auxValue)
        else:
            raise TypeError('_convertValueSimpleValueToPy(): Not supported value type (' + str(JType.toString()) + ')! Unable to get current value for some field...')

        # if we are dealing with an array2D, then we can extract
        # correctly the associated matrix with the right dimensions.
        # Remember Java is somehow row-major, Matlab is column-major, Python is row-major!
        if (JValue.getValueType().isArray2d()):
            """
                From JAPC - https://acc-sources.cern.ch/gitlab.cern.ch/acc-co/japc/japc-core/-/blob/japc-value/src/java/cern/japc/value/ValueType.java#L28:35
                public static boolean isArray2dType(ValueType valueType) {
                    return (valueType.equals(BOOLEAN_ARRAY_2D) || valueType.equals(BYTE_ARRAY_2D)
                        || valueType.equals(DOUBLE_ARRAY_2D) || valueType.equals(FLOAT_ARRAY_2D)
                        || valueType.equals(INT_ARRAY_2D) || valueType.equals(LONG_ARRAY_2D)
                        || valueType.equals(SHORT_ARRAY_2D) || valueType.equals(STRING_ARRAY_2D)     -> warning STRING_ARRAY_2D!
                        || valueType.equals(ENUM_ARRAY_2D) || valueType.equals(ENUM_SET_ARRAY_2D));  -> also isEnumeric()
            """
            if JType == self.STRING_ARRAY_2D or JValue.getValueType().isEnumeric():
                auxNColumns = JValue.getColumnCount()
                PyValue = [PyValue[i:i + auxNColumns] for i in range(0, len(PyValue), auxNColumns)]
            else:
                # it should be a numpy object....
                auxNRows = JValue.getRowCount()
                auxNColumns = JValue.getColumnCount()
                PyValue = np.reshape(PyValue, (auxNRows, auxNColumns))
            
        return PyValue
    
    def _createEmptyPyValueFromDesc(self, JValueDescriptor):
        """
        Creates a NaN numpy value starting from JValueDescriptor
        """

        print("WARNING _createEmptyPyValueFromDesc is a very simple starting point... to be improved....")

        if JValueDescriptor == None:
            # we start badly... assume a simple value
            PyValue = np.NaN
        elif JValueDescriptor.getType().hashCode() == self.SIMPLE:
            PyValue = np.NaN
        elif JValueDescriptor.getType().hashCode() == self.MAP:
            PyValue = dict()
            for name in JValueDescriptor.getNames():
                PyValue[name] = np.NaN
        else:
            raise TypeError('Unknown ValueDescriptor type!')
        return PyValue
    
    def PyToJValue(self, PyValue, JValueDescriptor):
        """
        #JNewValue = PyToJValue(PyValue, JValueDescriptor)
        # sets a python value (typically a numpy value or a dict of numpy values)
        # into a Java Parameter Value
        # if the JNewValue is a Map Parameter Value, then PyValue has to be a
        # dictionary with any of the key present in the current JNewValue
        # Map.
        """
        
        # import a SimpleParameterValueFactory
        cern = jp.JPackage("cern")
        SimpleParameterValueFactory = cern.japc.core.factory.SimpleParameterValueFactory
        MapParameterValueFactory    = cern.japc.core.factory.MapParameterValueFactory
        
        if JValueDescriptor == None:
            # try to just guess the type as SIMPLE and let
            JNewValue = self._convertSimplePyValueToSimpleJValue(PyValue, JValueDescriptor)
        else:
            # see if JNewValue is a map or a simple object
            JValueTypeHash = JValueDescriptor.getType().hashCode()
            if (JValueTypeHash == self.SIMPLE):
                JNewValue = self._convertSimplePyValueToSimpleJValue(PyValue, JValueDescriptor)
            elif (JValueTypeHash == self.MAP):
                # since this is a big structure, I need a struct as PyValue
                if not isinstance(PyValue, dict):
                    raise TypeError('PyToJValue:: you are trying to set a MAP parameter with a simple value. Impossible!')
                # here the names of the fields
                auxFieldNames = PyValue.keys(PyValue)
                
                # create a new Map Value based on descriptor
                JNewValue = MapParameterValueFactory.newValue(JValueDescriptor)
                
                # now set all of fields
                for auxFieldName, auxFieldValue in PyValue.items():
                    # get single field descriptor
                    auxJSingleValueDescriptor = JValueDescriptor.get(auxFieldName)
                    if isempty(auxJSingleValueDescriptor):
                        raise ValueError('PyToJValue:: no "' + auxFieldName + '" field in ' + str(JValueDescriptor.getName()))

                    # create simpleParameterValue with provided data
                    auxJSingleValue = self._convertSimplePyValueToSimpleJValue(auxFieldValue, auxJSingleValueDescriptor)
                    # insert this simpleParameterValue in the mapParameterValue
                    JNewValue.put(auxFieldName, auxJSingleValue)
            else:
                raise TypeError("Unknown Parameter type: {0}. No idea how to set that.".format(auxJSingleValueDescriptor.getType().toString()))
        return JNewValue
    
    def _getJavaValue(self, pyValueOrType):
        """Converts basic (numpy) python values to JPype type.
        if pyValueOrType is of class type, it returns the final JPype type 
        
        same as   def _getJavaValue(self, numpyType, pyValue=None): for PyJapc
        """
        # Lookup table. Input = Python type, output = [Python scalar type, JPype Java Type]
        typeLookup = {
            int:    [jp.JInt],
            float:  [jp.JDouble],
            bool:   [jp.JBoolean],
            str:    [jp.JString],

            np.int_:    [jp.JInt],
            np.float_:  [jp.JDouble],
            np.bool_:   [jp.JBoolean],
            np.str_:    [jp.JString],

            np.int8:    [int, jp.JByte],
            np.int16:   [int, jp.JShort],
            np.int32:   [int, jp.JInt],
            np.int64:   [int, jp.JLong],

            # Java does not have unsigned types, take the next bigger ones instead.
            np.uint8:   [int, jp.JShort],
            np.uint16:  [int, jp.JInt],
            np.uint32:  [int, jp.JLong],
            np.uint64:  [int, jp.JLong],

            np.float32: [float, jp.JFloat],
            np.float64: [float, jp.JDouble],

            # This will deserve some more attention
            np.ndarray: [jp.JArray]
        }

        pyType = type(pyValueOrType) 

        # Convert input list / tuple to a numpy array
        # 
        # This comes from _convertPyToSimpleValFallback. 
        #  Assumption here is that inside the list/tuple there is a simple/numerical data
        if pyType in (list, tuple):
            pyValueOrType = np.array(pyValueOrType)
        
        pyType = type(pyValueOrType) 
        if pyType not in typeLookup:
            raise TypeError("Python type {0} can not be converted to a JPype type".format(numpyType))
        
        if isinstance(pyValueOrType, type):
            return typeLookup[pyValueOrType][-1]
        elif isinstance(pyValueOrType, np.ndarray):
            # this is part of _convertPyToSimpleValFallback from PyJapc
            numpyDataType      = pyValueOrType.dtype.type
            numpyDataDimensions = pyValueOrType.ndim
            # In principle one could do:
            #   jValue = typeLookup[pyType](typeLookup[numpyDataType], numpyDataDimension)(pyVal.tolist())
            # but then I don't know how to inject it in JAPC if it turns out to be a 2D array... 
            # let's flatten it as done in PyJapc, and will think later ...
            jValue = typeLookup[pyType](typeLookup[numpyDataType], 1)(pyValueOrType.flatten().tolist())

        else:
            jValue = pyValueOrType
            for cast in typeLookup[pyType]:
                jValue = cast(jValue)
            return jValue


    def _getPyArrayToJavaDimensions(self, pyValue):
        """ Returns a _jarray.int[] with two elements if a 2D array or None if is a 1D object
        """
        pyType = type(pyValue) 
        if pyType in (list, tuple):
            pyValue = np.array(pyValue)
        
        if isinstance(pyValue, np.ndarray):
            numpyDataDimensions = pyValueOrType.ndim
            if numpyDataDimensions < 2:
                return None
            elif numpyDataDimensions == 2:
                # Store array shape in JAPC friendly format
                jArrayShape = jp.JArray(jp.JInt)(pyValue.shape)
                return jArrayShape
            else:
                raise TypeError("JAPC does only support arrays with <= 2 dimensions, {0} were given.".format(
                    pyValue.ndim)
                )
        else:
            return None


    def _convertSimplePyValueToSimpleJValue(self, PyValue, JValueDescriptor):
        """
        JNewValue = _convertSimplePyValueToSimpleJValue(PyValue, JValueDescriptor)
        # sets a python understandable value into a Java Simple Parameter Value

        Using MATLAB version + inputs from PyJAPC
        """

        # Python type of input variable
        pValT = type(PyValue)

        cern = jp.JPackage("cern")
        SimpleParameterValueFactory = cern.japc.core.factory.SimpleParameterValueFactory
        FunctionFactory             = cern.japc.value.factory.DomainValueFactory

        # In case the JValueType is not provided, I need to guess (could be improved)
        if JValueDescriptor == None:
            if isinstance(PyValue, dict):
                # ENUM or DISCRETE_FUNCTION 
                if ('X' in PyValue.keys) and ('Y' in PyValue.keys):
                    JValueTypeHash = self.DISCRETE_FUNCTION
                else:
                    JValueTypeHash = self.ENUM

            elif isinstance(PyValue, np.generic):
                # [TODO: VERY UGLY!!! MAYBE DEFINE GENERIC TYPE...] 
                # numpy scalar. assume double... 
                JValueTypeHash = self.DOUBLE
            elif isinstance(PyValue, np.ndarray) and np.issubdtype(PyValue.dtype, np.number):
                # numpy array of numbers
                if len(np.shape(PyValue)) > 1:
                    # 2D array
                    JValueTypeHash = self.DOUBLE_ARRAY_2D
                else:
                    JValueTypeHash = self.DOUBLE_ARRAY

            elif isinstance(PyValue, str):
                JValueTypeHash = self.STRING
            elif isinstance(PyValue, list):
                # could be strings .. but also DISCRETE_FUNCTION LIST or ENUM_SET
                if len(PyValue) > 0:
                    if isinstance(PyValue[0], str):
                        JValueTypeHash =self.STRING_ARRAY
                    elif isinstance(PyValue[0], dict):
                        # could be a DISCRETE_FUNCTION_LIST, ENUM_ARRAY, 
                        if ('X' in PyValue[0].keys()) and ('Y' in PyValue[0].keys()):
                            JValueTypeHash = self.DISCRETE_FUNCTION_LIST
                        elif len(PyValue[0].keys()) == 1 and PyValue[0].keys()[0].is_integer():
                            JValueTypeHash = self.ENUM_ARRAY
                        elif all(key.is_integer() for key in PyValue[0].keys()):
                            JValueTypeHash = self.ENUM_SET_ARRAY
                        else:
                            ValueError('No descriptor provided, and I did not manage to guess the type...')
                    elif isinstance(PyValue[0], list):
                        # ENUM_ARRAY_2D, ENUM_SET_ARRAY_2D
                        if len(PyValue[0]) > 0:
                            if isinstance(PyValue[0][0], dict):
                                if len(PyValue[0][0].keys()) == 1 and PyValue[0][0].keys()[0].is_integer():
                                    JValueTypeHash = self.ENUM_ARRAY_2D
                                elif all(key.is_integer() for key in PyValue[0][0].keys()):
                                    JValueTypeHash = self.ENUM_SET_ARRAY_2D
                                else:
                                    ValueError('No descriptor provided, and I did not manage to guess the type...')
                            elif isinstance(PyValue[0][0], str):
                                JValueTypeHash = self.STRING_ARRAY_2D
                            else:
                                ValueError('No descriptor provided, and I did not manage to guess the type...')
                        else:
                            raise ValueError('No descriptor and (probably) empty list provided... difficult to guess type...')
                    else:
                        try:
                            JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(PyValue)
                            return JNewValue
                        except:
                            print('I give up... but maybe possible to know what this is...')
                            raise
                else:
                    raise ValueError('No descriptor and empty list provided... difficult to guess type...')
            else:
                try:
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(PyValue)
                    return JNewValue
                except:
                    print('I give up... but maybe possible to know what this is...')
                    raise
        else:
            # Normal behavior. I can get valueType from descriptor
            JValueTypeHash = JValueDescriptor.getValueType().hashCode()
        
        # I also need to get the size, already in Java
        array2DSize = self._getPyArrayToJavaDimensions(PyValue)
        if array2DSize is not None:
            isArray2D = True
        else:
            isArray2D = False
        
        # properly create the new parameterValue

        ######## those simple values let handle by _getJavaValue directly ######
        # Maybe one could force the right destination type etc.. but too complicated...
        simpleTypes = (
            self.BYTE,      self.BYTE_ARRAY,    self.BYTE_ARRAY_2D,
            self.SHORT,     self.SHORT_ARRAY,   self.SHORT_ARRAY_2D,
            self.INT,       self.INT_ARRAY,     self.INT_ARRAY_2D,
            self.LONG,      self.LONG_ARRAY,    self.LONG_ARRAY_2D,
            self.FLOAT,     self.FLOAT_ARRAY,   self.FLOAT_ARRAY_2D,
            self.DOUBLE,    self.DOUBLE_ARRAY,  self.DOUBLE_ARRAY_2D,
            self.BOOLEAN,   self.BOOLEAN_ARRAY, self.BOOLEAN_ARRAY_2D,
            self.STRING,    self.STRING,        self.STRING_ARRAY_2D,
        )
        if (JValueTypeHash in simpleTypes):
            JValue = self._getJavaValue(PyValue)
            if isArray2D:
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JValue, array2DSize)
            else:
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JValue)

        ##### ENUMS ####
        elif (JValueTypeHash == self.ENUM):
            print(PyValue.keys()[0])
            print(PyValue[PyValue.keys()[0]])
            raise TypeError('ENUM TODO')
            """
                elif ts == "Enum":
                    parValNew = self._getSimpleValFromDesc(valueDescriptor)
                    # For now, enums can only be SET with an INT or STR
                    if isinstance(pyVal, six.string_types):
                        parValNew.setString(pyVal)
                    else:
                        parValNew.setInt(pyVal)
            """
        elif (JValueTypeHash == self.ENUM_ARRAY) or (JValueTypeHash == self.ENUM_ARRAY_2D):
            for auxValue1 in pyValue:
                for auxValue2 in auxValue:
                    print(PyValue.keys()[0])
                    print(PyValue[PyValue.keys()[0]])
                raise TypeError('ENUM ARRAY TODO')
        elif (JValueTypeHash == self.ENUM_SET):
            for key in PyValue.keys():
                print(key)
                print(PyValue[key])
            raise TypeError('ENUM SET TODO') 
            """
            parValNew = self._getSimpleValFromDesc(valueDescriptor)
            # For now, enumsets can only be SET with an INT.
            parValNew.setInt(pyVal)
            """
        elif (JValueTypeHash == self.ENUM_SET_ARRAY) or (JValueTypeHash == self.ENUM_SET_ARRAY_2D):
            raise TypeError('ENUM SET ARRAY TODO')

        ##### DISCRETE_FUNCTION ####
        elif (JValueTypeHash == self.DISCRETE_FUNCTION):
            """
                if isnumeric(PyValue)
                    # Per Georgess -> if numeric, just try with an array of doubles...
                    warn('matlabJAPC: Something strange here... will try to continue....')
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(double(PyValue))
            """
            df = FunctionFactory.newDiscreteFunction(np.array(PyValue['X'], dtype="double"), np.array(PyValue['Y'], dtype="double"))
            JNewValue = cern.japc.value.spi.value.simple.DiscreteFunctionValue(df)
        elif (JValueTypeHash == self.DISCRETE_FUNCTION_LIST):
            # Allocate JArray for DFs
            dfa = jp.JArray(jp.JPackage("cern").japc.value.DiscreteFunction)(len(pyValue))
            # Iterate over first dimension of user data
            for i, funcDat in enumerate(PyValue):
                funcDat2 = np.array(funcDat, dtype="double")
                dfa[i] = FunctionFactory.newDiscreteFunction(funcDat2[0, :], funcDat2[1, :])
            dfl = self._functionFactory.newDiscreteFunctionList(dfa)
            JNewValue = jp.JPackage("cern").japc.value.spi.value.simple.DiscreteFunctionListValue(dfl)
        else:
            raise ValueType('_convertSimplePyValueToSimpleJValue:: Not supported value type ('+str(JValueType.toString())+'), or something else went wrong')
        
        return JNewValue
