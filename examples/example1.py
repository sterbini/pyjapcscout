'''
re-writing in Python of example1 from Foteini Asvesta from 16/02/21

NOT TESTED!!!!
'''

# import PyJapcScout
from pyjapcscout import PyJapcScout

# to reload the data
from pyjapcscout import PyJapcScoutData

# some other imports for user use
import glob


# start PyJapcScout and so incaify Python instance
# matlabJapc.staticINCAify('PSB');
myPyJapc = PyJapcScout(incaAcceleratorName='PSB')

# Get and RBAC tocken
# matlabJapc.staticRBACAuthenticate()
myPyJapc.rbacLogin()

# Make a subscription
signalsToMonitor = [                       
                    'BR.QFO/LOG.OASIS.I_MEAS',
                    'BR.QDE/LOG.OASIS.I_MEAS',
                    'BR1.QCF/LOG.OASIS.I_MEAS', 
                    'BR1.QCD/LOG.OASIS.I_MEAS', 
                    'BR1.QCD3/LOG.OASIS.I_MEAS',                     
                    'BR1.QCD14/LOG.OASIS.I_MEAS', 
                    'BR2.QCF/LOG.OASIS.I_MEAS',                          
                    'BR2.QCD/LOG.OASIS.I_MEAS', 
                    'BR2.QCD3/LOG.OASIS.I_MEAS',                     
                    'BR2.QCD14/LOG.OASIS.I_MEAS', 
                    'BR3.QCF/LOG.OASIS.I_MEAS',                          
                    'BR3.QCD/LOG.OASIS.I_MEAS', 
                    'BR3.QCD3/LOG.OASIS.I_MEAS',                     
                    'BR3.QCD14/LOG.OASIS.I_MEAS', 
                    'BR4.QCF/LOG.OASIS.I_MEAS',                          
                    'BR4.QCD/LOG.OASIS.I_MEAS', 
                    'BR4.QCD3/LOG.OASIS.I_MEAS',                     
                    'BR4.QCD14/LOG.OASIS.I_MEAS', 
                    'BR14.MPS/LOG.OASIS.I_MEAS',
                    'BR23.MPS/LOG.OASIS.I_MEAS'
                    ]
mySelector = 'PSB.USER.TOF'

# in MATLAB one could define several monitors within one monitor object. This feature is not there in PyJapcScout              
#mon = ones(length(signalsToMonitor),1)';

# In MATLAB the callback function is typically in a separate file - not needed here!
#function  myCallback(data,h)
def myCallback(data, h):
    #disp(['Shot ' num2str(length(dir('2021*.mat')))]);
    print( 'Shot ' + str(len(glob.glob('2021*.mat'))) )

    PSB_user = 'PSB.USER.MD8'
    indx = len(glob.glob('2021*.mat')) + 1     

    if indx == 10:
        h.stop
        print('Measurement finished and monitor stopped.')


#myMonitor = matlabJapcMonitor(mySelector, ...
#    signalsToMonitor, @(data,h)myCallback(data,h),...
#    'Compensation',... % comment
#    mon);
#myMonitor.isTryingToGetValues=1; #NOT AVAILABLE IN PYJAPCSCOUT
#myMonitor.useFastStrategy(true); # -> groupStrategy = 'extended', instead of default 'standard'
myMonitor = myPyJapc.signalsToMonitor(parameters, onValueReceived=mycallback, selectorOverride = mySelector,
                        groupStrategy = 'extended',
                        allowManyUpdatesPerCycle=False, strategyTimeout=1200, forceGetOnChangeAndConstantValues=False)

# saving data configuration
myMonitor.saveDataPath = './Data/'
myMonitor.saveData = False
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'

# start acquisition
myMonitor.startMonitor()

return
## for controlling data acquisition:
#
myMonitor.saveData = True
#
myMonitor.saveData = False

## to stop the monitor
myMonitor.stopMonitor()


## to reload the data (here assuming parquet)
myData = PyJapcScoutData()
myData.load('./data/', fileFormat='parquet')
# everything in a Pandas dataframe:
myData.df
